# repo root path
repo_root=$(cd $(dirname $0) && pwd)

# other useful directories
export out=$repo_root/out # output directory
export rfs=$repo_root/initramfs # filesystem build directory

# remove initramfs directory if it exists
rm -rf $rfs

# create some needed directories within initramfs
mkdir -p $rfs/bin $rfs/sbin $rfs/dev $rfs/proc $rfs/sys

# go through the arguments (scripts) and run them
echo args: $*
for script in "$@"; do
  echo "running $script..."
  script_dir=$(cd $(dirname $script) && pwd) # directory containing the script
  $script $script_dir # pass the script it's directory to help it out
done

# create cpio archive of init filesystem
cd $rfs
find . | cpio -o -H newc > $out/init.cpio
echo "done!"

# clean up the $irfs build dir
rm $rfs -rf
