DO NOT USE. OLD FORK OF THE AKOZILAND INITRAMFS SCRIPTS

# Akoziland Initramfs

### Building
To build initramfs.cpio, simply run `build.sh req-scripts/create-init.sh req-scripts/busybox.sh` from any directory, output will end up in **out/**.

### Expanding
If you would like to easily expand onto the initramfs, without having to manage pulling changes made to the base, you can simply pass in extra scripts as arguments, and they will be run before the filesystem is compressed (you can either pass the absolute paths, or the path relative to this repo's root).

### Creating Extra Scripts
Any script passed to **build.sh** will be given the following arguments:
- `$1` path to the **directory containing the script**, useful if you need the script to grab other resources within it's directory
- `$rfs` path to **initramfs/**, this will be the **root directory on the booted system**
- `$out` path to the output directory, this is a level up from **initramfs/**, this is where you should build things before placing them in **initramfs/**, to make sure sure **initramfs/** stays clear of any uncleaned build artifacts (after all, we don't want to make a mess of someone's root directory)
