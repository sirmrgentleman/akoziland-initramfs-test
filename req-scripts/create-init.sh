echo "#!/bin/sh"                          > $rfs/init
echo "mount -t sysfs /sys"               >> $rfs/init
echo "mount -t proc proc /proc"          >> $rfs/init
echo "mount -t devtmpfs udev /dev"       >> $rfs/init
echo "sysctl -w kernel.printk='2 4 1 7'" >> $rfs/init
echo "clear"                             >> $rfs/init
echo "/bin/sh"                           >> $rfs/init
chmod +x                                    $rfs/init
