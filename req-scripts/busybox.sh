git clone --depth 1 https://git.busybox.net/busybox $out/busybox
cp $1/busybox.conf $out/busybox/.config
cd $out/busybox && make
make CONFIG_PREFIX=$rfs install
cd $out
rm $out/busybox -rf
